package com.budcamp.controllers;

import com.budcamp.entities.ForumPosts;
import com.budcamp.servicies.ForumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping
public class ForumController {

    @Autowired
    ForumService forumService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/controller/getposts")
    public ResponseEntity<List<ForumPosts>> getAllPosts() {
        return new ResponseEntity<List<ForumPosts>>(forumService.getAllPosts(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, params ="id")
    public ResponseEntity<ForumPosts> getPost(@RequestParam("id") int id){
        return new ResponseEntity<ForumPosts>(forumService.getPostById(id), HttpStatus.OK);
    }

/*
    @RequestMapping(method = RequestMethod.GET, value = "/controller/getposts")
    public ModelAndView getAllPosts() {
        List<ForumPosts> list = forumService.getAllPosts();
        ModelMap modelData = new ModelMap();
        modelData.addAttribute("post", list);
    return new ModelAndView("post", modelData);
}

    @RequestMapping(method = RequestMethod.GET, params ="id")
    public ResponseEntity<ForumPosts> getPost(@RequestParam("id") int id){
        return new ResponseEntity<ForumPosts>(forumService.getPostById(id), HttpStatus.OK);
    }
*/
}
