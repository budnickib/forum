package com.budcamp.repositories;

import com.budcamp.entities.ForumPosts;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ForumRepository extends CrudRepository<ForumPosts, Integer> {

    @Query("from ForumPosts as bp WHERE bp.id = :id")
    ForumPosts getPostById(@Param("id") int id);
}