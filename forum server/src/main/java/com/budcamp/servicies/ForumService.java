package com.budcamp.servicies;

import com.budcamp.entities.ForumPosts;
import com.budcamp.repositories.ForumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ForumService {
    @Autowired
    private ForumRepository PostRep;

    public List<ForumPosts> getAllPosts() {
        List<ForumPosts> list = new ArrayList<>();
        PostRep.findAll().forEach(list::add);
        return list;
    }

    public ForumPosts getPostById(int id) {
        return PostRep.getPostById(id);
    }

    public ForumPosts savePost(ForumPosts p){
        return PostRep.save(p);
    }
}
