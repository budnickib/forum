package com.budcamp.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "users")
public class ForumUsers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "userName")
    private String userName;

    public ForumUsers(int id, String login, String pass, String name) {
        this.id = id;
        this.login = login;
        this.password = pass;
        this.userName = name;
    }

    public ForumUsers(){}

    public ForumUsers(int id) {
        this.id = id;
    }

}
