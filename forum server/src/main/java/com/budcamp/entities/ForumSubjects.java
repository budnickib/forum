package com.budcamp.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "subject")
@Table(name = "subjects")
public class ForumSubjects {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "subject")
    private String subjectName;

    @Column(name = "date")
    private Date dateOfCreation;

    @Column(name = "createdBy")
    private int userOwnerId;

    public ForumSubjects(int id, String sName, Date dOCreation, int uOId) {
        this.id = id;
        this.subjectName = sName;
        this.dateOfCreation = dOCreation;
        this.userOwnerId = uOId;
    }

    public ForumSubjects(){}

    public ForumSubjects(int id) {
        this.id = id;
    }
}
