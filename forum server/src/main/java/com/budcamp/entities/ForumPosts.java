package com.budcamp.entities;

        import javax.persistence.*;
        import java.util.Date;
        import lombok.Data;

@Data
@Entity
@Table(name = "posts")
public class ForumPosts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    private Date date;

    @Column(name ="subject_id")
    private int subjectId;

    @Column(name="contents")
    private String contents;

    @Column(name="user_id")
    private int userId;

    public ForumPosts(String contents, Date date, int subjectId, int userId) {
        this.date = date;
        this.subjectId = subjectId;
        this.contents = contents;
        this.userId = userId;
    }

    public ForumPosts(){}

    public ForumPosts(String contents, int id, Date date, int subjectId, int userId) {
        this.id = id;
        this.date = date;
        this.subjectId = subjectId;
        this.contents = contents;
        this.userId = userId;
    }

    public ForumPosts(int id) {
        this.id = id;
    }
}
