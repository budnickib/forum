package com.forumClient;

import com.forumClient.Clients.ForumClient;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.client.RestTemplate;

public class ForumClientApplication {
	private static Logger logger = Logger.getLogger(ForumClientApplication.class);
	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("classpath:META-INF/spring/applicationContext.xml");
		ForumClient client = new ForumClient();
		client.getAllPosts(context.getBean(RestTemplate.class));
	}

}

