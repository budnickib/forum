package com.forumClient.Entities;

import java.util.Date;
import lombok.Data;

public @Data class ForumPosts {

    private int id;
    private Date date;
    private int subjectId;
    private String contents;
    private int userId;

    public ForumPosts(String contents, Date date, int subjectId, int userId) {
        this.date = date;
        this.subjectId = subjectId;
        this.contents = contents;
        this.userId = userId;
    }

    public ForumPosts(){}

    public ForumPosts(String contents, int id, Date date, int subjectId, int userId) {
        this.id = id;
        this.date = date;
        this.subjectId = subjectId;
        this.contents = contents;
        this.userId = userId;
    }

    public ForumPosts(int id) {
            this.id = id;
        }
}