package com.forumClient.Clients;

import com.forumClient.Entities.ForumPosts;
import org.apache.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class ForumClient {
    private static Logger logger = Logger.getLogger(ForumClient.class);

    public void getAllPosts(RestTemplate restTemplate){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);
        ParameterizedTypeReference<List<ForumPosts>> typeRef =
                new ParameterizedTypeReference<List<ForumPosts>>() {};

        ResponseEntity<List<ForumPosts>> response = restTemplate.exchange("http://localhost:8080/controller/getposts",
                HttpMethod.GET, requestEntity, typeRef);

        List<ForumPosts> list = response.getBody();
        System.out.println(list.get(0).getContents());
        System.out.println(list.get(1).getContents());
        System.out.println(list.get(2).getContents());
        logger.info(list.get(1));
    }
}
