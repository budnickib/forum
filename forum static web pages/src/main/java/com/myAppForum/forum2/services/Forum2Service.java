package com.myAppForum.forum2.services;

import com.myAppForum.forum2.entities.Forum2Posts;
import com.myAppForum.forum2.entities.Forum2Users;
import com.myAppForum.forum2.repositories.Forum2Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class Forum2Service {

    private static final Logger logger = Logger.getLogger("com.myAppForum.forum2.Forum2Service");

    @Autowired
    private Forum2Repository PostRep;

    public List<Forum2Posts> getAllPosts() {
        List<Forum2Posts> list = new ArrayList<>();
        PostRep.findAll().forEach(list::add);
        return list;
    }

    public Forum2Posts getPostById(int id) {
        return PostRep.getPostById(id);
    }

    public List<Forum2Posts> getAllUserPosts(String login){
        List<Forum2Posts> list = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();
        int id = PostRep.getUserIdByName(login);
        list = PostRep.getAllUsersPosts(id);
        return list;
    }

    public Forum2Posts savePost(Forum2Posts p){
        return PostRep.save(p);
    }

    public Forum2Users getUser(String login) {return PostRep.getUserByName(login);}
}
