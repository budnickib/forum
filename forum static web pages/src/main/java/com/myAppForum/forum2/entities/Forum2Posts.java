package com.myAppForum.forum2.entities;

import javax.persistence.*;
import java.util.Date;
import lombok.Data;

@Data
@Entity
@Table(name = "posts")
public class Forum2Posts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    private Date date;

    @Column(name ="subject_id")
    private int subjectId;

    @Column(name="contents")
    private String contents;

    @Column(name="user_id")
    private int userId;

    public Forum2Posts(Date date, int subjectId, String contents, int userId) {
        this.date = date;
        this.subjectId = subjectId;
        this.contents = contents;
        this.userId = userId;
    }

    public Forum2Posts(){}

    public Forum2Posts(int id, Date date, int subjectId, String contents, int userId) {
        this.id = id;
        this.date = date;
        this.subjectId = subjectId;
        this.contents = contents;
        this.userId = userId;
    }

    public Forum2Posts(int id) {
        this.id = id;
    }
}
