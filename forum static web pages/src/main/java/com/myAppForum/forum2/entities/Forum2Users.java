package com.myAppForum.forum2.entities;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
public class Forum2Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "user_name")
    private String userName;

    public Forum2Users(int id, String login, String pass, String name) {
        this.id = id;
        this.login = login;
        this.password = pass;
        this.userName = name;
    }


    public Forum2Users(){}

    public Forum2Users(int id) {
        this.id = id;
    }
}