package com.myAppForum.forum2.controller;

import com.myAppForum.forum2.entities.Forum2Posts;
import com.myAppForum.forum2.entities.Forum2Users;
import com.myAppForum.forum2.services.Forum2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/post")
public class Forum2Controller {

    private static final Logger logger = Logger.getLogger("com.myAppForum.forum2.Forum2Controller");
    @Autowired
    private Forum2Service forum2Service;

    @RequestMapping(value = "/postlist", method = RequestMethod.GET)
    public ModelAndView listFixedDeposits(Principal principal) {
        Map<String, List<Forum2Posts>> modelData = new HashMap<String, List<Forum2Posts>>();
        //logger.info("lista: "+ principal.getName());
        List<Forum2Posts> list = forum2Service.getAllUserPosts(principal.getName());
        modelData.put("List", list);
        return new ModelAndView("postList", modelData);
    }

    @RequestMapping(value = "/newpost", method = RequestMethod.GET)
    public ModelAndView createFormPost(){
        Map<String, Object> modelData = new HashMap<>();
        return new ModelAndView("createPost", modelData);
    }

    @RequestMapping(params = "fAction=createPost", method = RequestMethod.POST)
    public ModelAndView createPost(HttpServletRequest request){
        String text = request.getParameter("text");
        String name = request.getUserPrincipal().getName();
        Forum2Users user = forum2Service.getUser(name);
        Forum2Posts post = new Forum2Posts(new Date(),2, text, user.getId());
        forum2Service.savePost(post);
        return listFixedDeposits(request.getUserPrincipal());
    }

    @ExceptionHandler
    public String handleException(Exception ex) {
        return "error";
    }

}