package com.myAppForum.forum2.repositories;

import com.myAppForum.forum2.entities.Forum2Posts;
import com.myAppForum.forum2.entities.Forum2Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Forum2Repository extends CrudRepository<Forum2Posts, Integer> {

    @Query("from Forum2Posts as bp WHERE bp.id = :id")
    Forum2Posts getPostById(@Param("id") int id);

    @Query("SELECT id from Forum2Users as fu where fu.login= :name")
    int getUserIdByName(@Param("name") String name);

    @Query("from Forum2Posts fp where fp.userId= :user_id")
    List<Forum2Posts> getAllUsersPosts(@Param("user_id") int user_id);

    @Query("from Forum2Users fu where fu.login = :login")
    Forum2Users getUserByName(@Param("login") String login);
// it is possible to create methods without @Query adnotation
    
    public int countById(int id);

    List<Forum2Posts> findByContentsContainsAllIgnoreCase(String contentsPart);
}