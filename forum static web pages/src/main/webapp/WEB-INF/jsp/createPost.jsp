<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% response.setCharacterEncoding("UTF-8"); request.setCharacterEncoding("UTF-8"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
    <head>
        <title>Nowy post</title>
            <style type="text/css">
                .td {
	                font-family: 'arial';
	                font-size: 12px;
                	vertical-align: top;
                }
            </style>
    </head>

    <body>
        <form name="newpost" method="POST" action="${pageContext.request.contextPath}/post?fAction=createPost">
            <table align="left" style="padding-left: 300px;">
            			<tr>
            				<td style="font-family: 'arial'; font-size: 16px; font-weight: bold;" align="left">
            				    Nowy post
            				</td>
            			</tr>
            			<tr align="left">
            				<td>
            					<table class="border" cellpadding="10">
            						<tr>
            							<td class="td"><b>Temat:</b></td>
            							<td class="td">
            							    <input type="text" name="subject"/>
            							</td>
            						</tr>
            						<tr>
            							<td class="td"><b>Tekst:</b></td>
            							<td class="td">
            							    ​<textarea id="txtArea" name="text" rows="10" cols="70"></textarea>
            							</td>
            						</tr>
            					</table>
            				</td>
            			</tr>
            			<tr align="left">
            				<td>
            					<table style="padding-left: 100px;">
            						<tr align="center">
            							<td class="td"><input type="submit" value="Zapisz" />
            						</tr>
            					</table>
            				</td>
            			</tr>
            		</table>

        </form>
    </body>
</html>