<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% response.setCharacterEncoding("UTF-8"); request.setCharacterEncoding("UTF-8"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title>Lista Postów</title>
<style type="text/css">
.border {
	border-width: 1px;
	border-style: solid;
	border-collapse: collapse;
}

.td,.th {
	border: 1px solid;
	font-family: 'arial';
	font-size: 12px;
}

.a {
	font-family: 'arial';
	font-size: 12px;
}
</style>
</head>
<body>
	<form name="PostList" method="GET"
		action="${pageContext.request.contextPath}/post/newpost">
		<table align="left" style="padding-left: 300px;">
		    <tr>
        		<td style="font-family: 'arial'; font-size: 12px; font-weight: bold" align="right">
        			<a href="${pageContext.request.contextPath}/logout">Wyloguj</a>
        				<p>
        					Użytkownik: <sec:authorize access="isAuthenticated()">
                                          <sec:authentication property="name"/>
                                        </sec:authorize>
        				</p>
        		</td>
        	</tr>
			<tr>
				<td
					style="font-family: 'arial'; font-size: 16px; font-weight: bold;">Lista Postów</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<table class="border" cellpadding="10">
						<tr bgcolor="#99CCFF">
						    <th class="th">Id</th>
							<th class="th">Data utworzenia</th>
							<th class="th">id tematu</th>
							<th class="th">treść</th>
							<th class="th">id użytkownika</th>
							<th class="th">opcje</th>
						</tr>
						<c:forEach items="${List}" var="forum2Posts">
							<tr>
								<td class="td"><c:out value="${forum2Posts.id}" /></td>
								<td class="td"><c:out value="${forum2Posts.date}" /></td>
								<td class="td"><c:out value="${forum2Posts.subjectId}" /></td>
								<td class="td"><c:out value="${forum2Posts.contents}" /></td>
								<td class="td"><c:out value="${forum2Posts.userId}" /></td>
								<td class="td"><a
									<a href="${pageContext.request.contextPath}/"
									style="color: green">Edytuj</a></td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</tr>
			<tr align="center">
				<td><input type="submit" value="Dodaj posta" /></td>
			</tr>
		</table>
	</form>
</body>
</html>